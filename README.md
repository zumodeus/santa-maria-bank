# **Santa Maria Bank**

<img src="https://img.shields.io/badge/language-C%2B%2B-blue">
<img src="https://img.shields.io/badge/version-1.0.1--2022-green">
<img src="https://img.shields.io/badge/tools-QT Creator%20|%20VSCode%20|%20SQLite-brightgreen">

### **`Introducción`**

El presente proyecto es un banco simulado, en el cual se pueden realizar una cantidad de acciones tales como transferencias, abonos, revision del historial, etc.

#### **`Uso`**

El uso del programa inicia con la creación de una cuenta, en esta deberas registrarte con **NOMBRE, APELLIDO, EMAIL Y CONTRASEÑA**, despues te logueas con el nuevo email y contraseña, al momento de ingresar tendrás la interfaz de tu cuenta creada, donde podrás transferir dinero, o abonar a la cuenta. 

**Trasferencias**:

- Primero deberás irte al apartado de **TRANSFERIR**,una vez ahí necesitaras asegurarte que la cuenta a la que vas a enviar esté registrada (esto lo alertará el programa si se intenta transferir dinero a una cuenta no existente) y que el monto no exceda tu dinero actual.
- Si recibiste el dinero, no olvides recargar el programa con el botón que está a la izquierda de tu saldo.

**Abonos:**
- Para abonar dinero a tu cuenta deberás irte al apartado **ABONAR**, despues ingresar la cantidad de dinero que estimes conveniente abonar a la cuenta.

El programa cuenta con una cartola, la cual muestra las ultimas transferencias realizadas en tu cuenta, mostrando **MONTO** y **FECHA (AAAA/MM/DD | HH:SS:MS)**.

#### **`Compilación`**

Para compilar el proyecto se debe hacer uso del siguiente comando:

```sh
qmake
```

Y luego:

```sh
make
```

Finalmente todo este procedimiento generara un binario ejecutable.

#### **`Ejecución`**

Como se dijo en el punto anterior, la compilacion generara un binario, el cual debera ser ejecutado con el siguiente comando:

```sh
# MacOS
open SantaMariaBank.app

# Linux
./SantaMariaBank

# Windows
start [SantaMariaBank.exe]
```

#### **`UX/UI Section`**

**`1. Login:`**

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001653069613903952/Captura_de_Pantalla_2022-07-26_a_las_20.42.19.png">

**`2. SignUp:`**

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001653070318542940/Captura_de_Pantalla_2022-07-26_a_las_20.42.44.png">

**`3. Dashboard (Transfer):`**

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001653070708625439/Captura_de_Pantalla_2022-07-26_a_las_20.44.09.png">

**`4. Dashboard (Deposit):`**

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001653071119659028/Captura_de_Pantalla_2022-07-26_a_las_20.44.41.png">

**`5. History Table:`**

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001653072151445544/Captura_de_Pantalla_2022-07-26_a_las_20.49.52.png">

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001653072600256612/Captura_de_Pantalla_2022-07-26_a_las_20.50.00.png">

#### **`Diagramas`**

**`1. Componentes:`**

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001657874273026099/Diagrama_en_blanco_2.png">

**`2. Secuencia:`**

<img src="https://cdn.discordapp.com/attachments/966182229757812756/1001657874566623232/Diagrama_de_secuencia_basico.png">

### **`Desarrolladores`**

**`Vicente Benjamín Zúñiga Montenegro`**:
- [**`Instagram`**](https://www.instagram.com/zumodeus)
- [**`LinkedIn`**](https://www.linkedin.com/in/zumodeus)

**`ROL: 202.130.526-2`**

<hr>

**`Juan Chavez Toledo`**:
- [**`Instagram`**](https://www.instagram.com/silencee.wannabe)

**`ROL: 202.030.501-3`**

### **`Version`**

```sh
Santa Maria Bank ® | v1.0.1-2022
```
