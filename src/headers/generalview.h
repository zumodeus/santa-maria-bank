#ifndef GENERALVIEW_H
#define GENERALVIEW_H

#include <QtGui>
#include <QTimer>
#include <QtWidgets>
#include <QMainWindow>
#include <QGraphicsScene>
#include "src/headers/database.h"

QT_BEGIN_NAMESPACE
namespace Ui { class GeneralView; }
QT_END_NAMESPACE

class GeneralView : public QMainWindow
{
    Q_OBJECT

public:
    GeneralView(QWidget *parent = nullptr);
    void HandleError(QString);
    void AddDataToTable();
    void UserOperationSwitch(bool, bool);
    void AuthenticationSwitch(bool, bool);
    ~GeneralView();

private slots:

    // Switchs
    void SignUpSwitch();
    void LogInSwitch();
    void TransferSwitch();
    void ReloadSwitch();

    // Authentication
    void SignUpAction();
    void LogInAction();
    void LogOutAction();
    void TransferAction();
    void ReloadAction();

    // Force Reload
    void ForceReload();

private:
    User * user;
    Database * db;
    Ui::GeneralView * ui;
};
#endif // GENERALVIEW_H
