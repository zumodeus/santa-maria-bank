#ifndef DATABASE_H
#define DATABASE_H

#include <QSql>
#include <string>
#include <vector>
#include <QDebug>
#include <QString>
#include <QSqlError>
#include <iostream>
#include <QSqlQuery>
#include <QSqlDatabase>

typedef struct {
    long balance;
    std::string name;
    std::string surname;
    std::string email;
} User;

typedef struct {
    std::string to;
    std::string from;
    int amount;
    std::string date;
} History;

class Database {

    private:
        QSqlDatabase db;

    public:
        Database(std::string);
        bool SetUser(std::string, std::string, std::string, std::string);
        User * GetUser(std::string, std::string, bool);
        std::vector<History> GetHistory(std::string);
        bool TransactionOperation(std::string, std::string, int);
        ~Database();
};

#endif // DATABASE_H
