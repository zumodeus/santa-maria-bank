#include "src/headers/generalview.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    GeneralView w;
    w.setFixedHeight(620);
    w.setFixedWidth(440);
    w.show();
    return a.exec();
}
