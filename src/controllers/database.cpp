#include "src/headers/database.h"

Database::Database(std::string db) {
    this->db = QSqlDatabase::addDatabase("QSQLITE");
    this->db.setDatabaseName(db.c_str());
    this->db.open();
    if (!this->db.isValid()) throw -1;
    QSqlQuery query(this->db);
    query.prepare("CREATE TABLE IF NOT EXISTS user ("\
                      "balance LONG," \
                      "name VARCHAR," \
                      "surname VARCHAR," \
                      "email VARCHAR," \
                      "password VARCHAR" \
                  ");");
    if (!query.exec())
        throw -1;
    query.prepare("CREATE TABLE IF NOT EXISTS history ("\
                      "_to VARCHAR," \
                      "_from VARCHAR," \
                      "amount INTEGER," \
                      "created_at TIMESTAMP" \
                  ");");
    if (!query.exec())
       throw -1;
};

User * Database::GetUser(std::string email, std::string pass, bool mode = false) {
    User * response = NULL;
    QSqlQuery query(this->db);
    query.prepare("SELECT balance, name, surname, email, password FROM user WHERE email = ?");
    query.addBindValue(email.c_str());
    if (query.exec() && query.next()) {
        response = new User;
        response->balance = query.value(0).toInt();
        response->name = query.value(1).toString().toStdString();
        response->surname = query.value(2).toString().toStdString();
        response->email = query.value(3).toString().toStdString();
        if (mode ? false : pass != query.value(4).toString().toStdString()) {
            delete response;
            response = NULL;
        }
    }
    return response;
};

bool Database::SetUser(std::string name, std::string surname, std::string email, std::string password) {
    QSqlQuery query(this->db);
    query.prepare("SELECT COUNT(*) FROM user WHERE email = ?;");
    query.addBindValue(QString::fromStdString(email));
    if (query.exec())
        if (query.next() && query.value(0).toInt() != 0)
            return false;
    query.clear();
    query.prepare("INSERT INTO user(balance, name, surname, email, password) VALUES (:balance, :name, :surname, :email, :password);");
    query.bindValue(":balance", 6660);
    query.bindValue(":name", name.c_str());
    query.bindValue(":surname", surname.c_str());
    query.bindValue(":email", email.c_str());
    query.bindValue(":password", password.c_str());
    return query.exec() && query.numRowsAffected();
};

std::vector<History> Database::GetHistory(std::string email) {
    std::vector<History> response;
    QSqlQuery query(this->db);
    query.prepare("SELECT * FROM history WHERE _to = :to OR _from = :from ORDER BY created_at DESC");
    query.bindValue(":to", QString::fromStdString(email));
    query.bindValue(":from", QString::fromStdString(email));
    if (query.exec()) {
        while (query.next()) {
            History hist;
            hist.to = query.value(0).toString().toStdString();
            hist.from = query.value(1).toString().toStdString();
            hist.amount = query.value(2).toInt();
            hist.date = query.value(3).toString().toStdString();
            response.push_back(hist);
        }
    };
    return response;
};

bool Database::TransactionOperation(std::string from, std::string to, int amount) {
    QSqlQuery query(this->db);
    query.prepare("SELECT COUNT(*) FROM user WHERE email = ?;");
    query.addBindValue(QString::fromStdString(to));
    if (query.exec())
        if (query.next() && query.value(0).toInt() != 1)
            return false;
    query.clear();
    if (to != from) {
        query.prepare("SELECT balance FROM user WHERE email = ?;");
        query.addBindValue(QString::fromStdString(from));
        if (query.exec())
            if (query.next() && query.value(0).toInt() < amount)
                return false;
        query.clear();
    }
    query.prepare("INSERT INTO history(_to, _from, amount) VALUES (:to, :from, :amount);");
    query.bindValue(":to", QString::fromStdString(to));
    query.bindValue(":from", QString::fromStdString(from));
    query.bindValue(":amount", amount);
    if (query.exec())
        if (!query.numRowsAffected())
            return false;
    query.clear();
    query.prepare("UPDATE user SET balance = balance + ? WHERE email = ?;");
    query.addBindValue(amount);
    query.addBindValue(QString::fromStdString(to));
    if (query.exec())
        if (!query.numRowsAffected())
            return false;
    query.clear();
    if (to != from) {
        query.prepare("UPDATE user SET balance = balance - ? WHERE email = ?");
        query.addBindValue(amount);
        query.addBindValue(QString::fromStdString(from));
        if (query.exec())
            if (!query.numRowsAffected())
                return false;
    }
    return true;
};

Database::~Database() {
    this->db.close();
};
