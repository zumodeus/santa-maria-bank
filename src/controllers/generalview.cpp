#include "src/headers/generalview.h"
#include "ui_generalview.h"

GeneralView::GeneralView(QWidget *parent)
    : QMainWindow(parent)
    , user(NULL)
    , db(new Database("usm_bank.db"))
    , ui(new Ui::GeneralView)
{
    ui->setupUi(this);
    ui->history_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->error_log->setVisible(false);
    ui->transfer_switch->setDisabled(true);
    ui->reload_widget->setVisible(false);
    ui->user_widget->setVisible(false);
    ui->signup_widget->setVisible(false);

    ui->history_table->setRowCount(0);
    ui->history_table->setColumnCount(4);
    ui->history_table->setColumnWidth(0, 200);
    ui->history_table->setColumnWidth(1, 200);
    ui->history_table->setColumnWidth(2, 200);
    ui->history_table->setColumnWidth(3, 200);
    ui->history_table->setHorizontalHeaderLabels({ "Para", "Desde", "Monto", "Fecha" });

    connect(ui->login_switch, SIGNAL(clicked()), this, SLOT(LogInSwitch()));
    connect(ui->signup_switch, SIGNAL(clicked()), this, SLOT(SignUpSwitch()));
    connect(ui->login_button, SIGNAL(clicked()), this, SLOT(LogInAction()));
    connect(ui->signup_button, SIGNAL(clicked()), this, SLOT(SignUpAction()));
    connect(ui->transfer_switch, SIGNAL(clicked()), this, SLOT(TransferSwitch()));
    connect(ui->reload_switch, SIGNAL(clicked()), this, SLOT(ReloadSwitch()));
    connect(ui->logout_button, SIGNAL(clicked()), this, SLOT(LogOutAction()));
    connect(ui->force_reload, SIGNAL(clicked()), this, SLOT(ForceReload()));
    connect(ui->reload_button, SIGNAL(clicked()), this, SLOT(ReloadAction()));
    connect(ui->transfer_button, SIGNAL(clicked()), this, SLOT(TransferAction()));
}

void GeneralView::HandleError(QString context) {
    ui->error_log->show();
    ui->error_log->setText(context);
    QTimer::singleShot(5000, ui->error_log, SLOT(hide()));
};

void GeneralView::AddDataToTable() {
    int i;
    History auxiliar;
    std::vector<History> history = db->GetHistory(user->email);
    ui->history_table->clearContents();
    for (i=0; i<(int) history.size(); i++) {
        auxiliar = history[i];
        ui->history_table->insertRow(i);
        ui->history_table->setRowCount(i + 1);
        ui->history_table->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(auxiliar.to)));
        ui->history_table->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(auxiliar.from)));
        ui->history_table->setItem(i, 2, new QTableWidgetItem("$ " + QString::fromStdString(std::to_string(auxiliar.amount))));
        ui->history_table->setItem(i, 3, new QTableWidgetItem(QString::fromStdString(auxiliar.date)));
    };
};

void GeneralView::AuthenticationSwitch(bool mode, bool over = false) {
    ui->login_widget->setVisible(over ? false : mode);
    ui->signup_widget->setVisible(over ? false : !mode);
    ui->user_widget->setVisible(over);
    if (over) {
        QString banner = "Bienvenido, " + QString::fromStdString(user->name) + " " + QString::fromStdString(user->surname);
        ui->user_banner->setText(banner);
        QString balance = QString::fromStdString(std::to_string(user->balance));
        ui->balance->setText(balance);
        GeneralView::AddDataToTable();
    }
};

void GeneralView::UserOperationSwitch(bool mode, bool over = false) {
    if (over) {
        delete user;
        user = NULL;
        ui->user_widget->setVisible(false);
        ui->login_widget->setVisible(true);
    }
    ui->transfer_switch->setDisabled(over ? true : mode);
    ui->transfer_widget->setVisible(over ? true : mode);
    ui->reload_switch->setDisabled(over ? false : !mode);
    ui->reload_widget->setVisible(over ? false : !mode);
};

void GeneralView::SignUpSwitch() {
    ui->email_input->setText("");
    ui->pass_input->setText("");
    GeneralView::AuthenticationSwitch(false);
}

void GeneralView::LogInSwitch() {
    ui->name_input->setText("");
    ui->surname_input->setText("");
    ui->email_input_s->setText("");
    ui->pass_input_s->setText("");
    GeneralView::AuthenticationSwitch(true);
};

void GeneralView::TransferSwitch() {
    GeneralView::UserOperationSwitch(true);
};

void GeneralView::ReloadSwitch() {
    GeneralView::UserOperationSwitch(false);
};

void GeneralView::SignUpAction() {
    if (ui->name_input->text().isEmpty() ||
        ui->surname_input->text().isEmpty() ||
        ui->email_input_s->text().isEmpty() ||
        ui->pass_input_s->text().isEmpty()) {
        GeneralView::HandleError("Debes completar todos los campos.");
        return;
    }
    std::string name = ui->name_input->text().toStdString(),
                surname = ui->surname_input->text().toStdString(),
                email = ui->email_input_s->text().toStdString(),
                password = ui->pass_input_s->text().toStdString();
    if (db->SetUser(name, surname, email, password)) {
        GeneralView::LogInSwitch();
        return;
    }
    GeneralView::HandleError("Error al crear la cuenta.");
};

void GeneralView::LogInAction() {
    if (ui->email_input->text().isEmpty() ||
        ui->pass_input->text().isEmpty()) {
        GeneralView::HandleError("Debes completar todos los campos.");
        return;
    }
    std::string uEmail = ui->email_input->text().toStdString();
    std::string uPass = ui->pass_input->text().toStdString();
    User * data = db->GetUser(uEmail, uPass, false);
    if (data != NULL) {
        user = data;
        ui->email_input->setText("");
        ui->pass_input->setText("");
        GeneralView::AuthenticationSwitch(false, true);
        return;
    }
    GeneralView::HandleError("Problema al iniciar sesion, intenta otra vez.");
};

void GeneralView::LogOutAction() {
    ui->history_table->setRowCount(0);
    GeneralView::UserOperationSwitch(false, true);
};

void GeneralView::TransferAction() {
    int amount = ui->amount_input->text().toInt();
    std::string to = ui->to_input->text().toStdString();
    if (amount <= 0) {
        GeneralView::HandleError("Ingresa un monto valido.");
        return;
    }
    if (db->TransactionOperation(user->email, to, amount)) {
        ui->amount_input->setText("");
        ui->to_input->setText("");
        GeneralView::ForceReload();
        return;
    }
    GeneralView::HandleError("Error al realizar la transaccion.");
};

void GeneralView::ReloadAction() {
    int amount = ui->amount_input_a->text().toInt();
    if (amount <= 0) {
        GeneralView::HandleError("Ingresa un monto valido.");
        return;
    }
    if (db->TransactionOperation(user->email, user->email, amount)) {
        ui->amount_input_a->setText("");
        GeneralView::ForceReload();
        return;
    }
    GeneralView::HandleError("Error al realizar la transaccion.");
};

void GeneralView::ForceReload() {
    User * data = db->GetUser(user->email, "", true);
    if (data != NULL) {
        delete user;
        user = data;
        QString balance = QString::fromStdString(std::to_string(user->balance));
        ui->balance->setText(balance);
        GeneralView::AddDataToTable();
        return;
    }
    GeneralView::HandleError("Vuelve a intentarlo.");
};

GeneralView::~GeneralView() {
    if (user != NULL) delete user;
    delete ui;
}
